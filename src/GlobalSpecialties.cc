#include "GlobalSpecialties.h"
#include "EthernetConnection.h"
#include "SerialConnection.h"
#include <iostream>
#include <string>
#include <fstream>
#include <unistd.h>

GlobalSpecialties::GlobalSpecialties(const pugi::xml_node configuration) : PowerSupply("GlobalSpecialties", configuration) { configure(); }
GlobalSpecialties::~GlobalSpecialties()
{
}

void GlobalSpecialties::configure()
{
    std::cout << "Configuring GlobalSpecialties ..." << std::endl;
    std::string connectionType = fConfiguration.attribute("Connection").as_string();
    // int         timeout        = fConfiguration.attribute("Timeout").as_int();
    std::string port           = fConfiguration.attribute("Port").as_string();
    fSeriesName                = fConfiguration.attribute("Series").as_string();
    std::cout << "Connection type: " << connectionType << " Series: " << fSeriesName << std::endl;
    if(fSeriesName.compare("1368") != 0)
    {
        std::stringstream error;
        error << "GlobalSpecialties configuration: the series named " << fSeriesName << " has no code implemented, aborting...";
        throw std::runtime_error(error.str());
    }
    if(std::string(fConfiguration.attribute("Connection").value()).compare("TMC") != 0)
    {
        std::stringstream error;
        error << "GlobalSpecialties: Cannot implement connection type " << fConfiguration.attribute("Connection").value() << ".\n"
              << "Possible values are Serial or Ethernet";
        throw std::runtime_error(error.str());
    }

    for(pugi::xml_node channel = fConfiguration.child("Channel"); channel; channel = channel.next_sibling("Channel"))
    {
        std::string inUse = channel.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string id = channel.attribute("ID").value();
        std::cout << __PRETTY_FUNCTION__ << "Configuring channel: " << id << std::endl;
        PowerSupply::fChannelMap.emplace(id, new GlobalSpecialtiesChannel(port, channel, fSeriesName));
    }
}

GlobalSpecialtiesChannel::GlobalSpecialtiesChannel(std::string port, const pugi::xml_node configuration, std::string seriesName)
    : PowerSupplyChannel(configuration), fPort(port), fChannel(fConfiguration.attribute("Channel").value()), fSeriesName(seriesName)
{
    fChannelCommand = std::string("INST:NSEL ") + fChannel.substr(2);
}
GlobalSpecialtiesChannel::~GlobalSpecialtiesChannel() {}

/*!
************************************************
 * Sets the channel that needs to be accessed.
 \param command Command to be send.
************************************************
*/
void GlobalSpecialtiesChannel::setChannel(void) 
{
    std::ofstream device(fPort);
    if (!device) {
        std::cerr << "Error: Unable to open " << fPort << std::endl;
        return;
    }

    device << fChannelCommand << "\n";
    device.flush();
}

/*!
************************************************
 * Sends write command to connection.
 \param command Command to be send.
************************************************
*/
void GlobalSpecialtiesChannel::write(std::string command) 
{
    // std::cout << "Setting channel " << fChannel << std::endl;
    setChannel();
    // std::cout << "Sending command: " << command << std::endl;
    std::ofstream device(fPort);
    if (!device) {
        std::cerr << "Error: Unable to open " << fPort << std::endl;
        return;
    }

    device << command << "\n";  // Send command
    device.flush();
    // std::cout << "Command sent: " << command << std::endl;
}

/*!
************************************************
 * Sends read command to connection.
 \param command Command to be send.
************************************************
*/
std::string GlobalSpecialtiesChannel::read(std::string command)
{
    write(command);
    std::ifstream device(fPort);
    if (!device) {
        std::cerr << "Error: Unable to read from " << fPort << std::endl;
        return "";
    }

    std::string response;
    std::getline(device, response);
    return response;
}

void GlobalSpecialtiesChannel::turnOn()
{
    write("OUTP ON");
    // std::cout << "Turn on channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

void GlobalSpecialtiesChannel::turnOff()
{
    write("OUTP OFF");
    // std::cout << "Turn off channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

bool GlobalSpecialtiesChannel::isOn()
{
    std::string answer = read("OUTP?");
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

void GlobalSpecialtiesChannel::setVoltage(float voltage) { write("VOLT " + std::to_string(voltage)); }

void GlobalSpecialtiesChannel::setCurrent(float current) { setCurrentCompliance(current); }

float GlobalSpecialtiesChannel::getOutputVoltage()
{
    std::string answer = read("VOLT?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

float GlobalSpecialtiesChannel::getSetVoltage() { return getVoltageCompliance(); }

float GlobalSpecialtiesChannel::getCurrent()
{
    std::string answer = read("CURR?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

void GlobalSpecialtiesChannel::setVoltageCompliance(float voltage) { setVoltage(voltage); }

void GlobalSpecialtiesChannel::setCurrentCompliance(float current) { write("CURR " + std::to_string(current)); }

float GlobalSpecialtiesChannel::getVoltageCompliance()
{
    std::string answer = read("VOLT?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

float GlobalSpecialtiesChannel::getCurrentCompliance()
{
    std::string answer = read("CURR?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

void GlobalSpecialtiesChannel::setOverVoltageProtection(float maxVoltage) { write("VOLT:PROT " + std::to_string(maxVoltage)); }

void GlobalSpecialtiesChannel::setOverCurrentProtection(float maxCurrent)
{
    std::stringstream error;
    error << "GlobalSpecialties setOverCurrentProtection method not implemented, "
             "aborting...";
    throw std::runtime_error(error.str());
}

float GlobalSpecialtiesChannel::getOverVoltageProtection()
{
    std::string answer = read("VOLT:PROT?");
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

float GlobalSpecialtiesChannel::getOverCurrentProtection()
{
    std::stringstream error;
    error << "GlobalSpecialties getOverCurrentProtection method not implemented, "
             "aborting...";
    throw std::runtime_error(error.str());
}

void GlobalSpecialtiesChannel::setParameter(std::string parName, float value) { write(parName + " " + std::to_string(value)); }

void GlobalSpecialtiesChannel::setParameter(std::string parName, bool value) { write(parName + " " + std::to_string(value)); }

void GlobalSpecialtiesChannel::setParameter(std::string parName, int value) { write(parName + " " + std::to_string(value)); }

float GlobalSpecialtiesChannel::getParameterFloat(std::string parName)
{
    std::string answer = read(parName);
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

int GlobalSpecialtiesChannel::getParameterInt(std::string parName)
{
    std::string answer = read(parName);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

bool GlobalSpecialtiesChannel::getParameterBool(std::string parName)
{
    std::string answer = read(parName);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}
