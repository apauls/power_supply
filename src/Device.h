/*!
 * \authors Rudy Ceccarelli <rudy.ceccarelli@cern.ch>, INFN-Firenze
 * \date Jan 3 2023
 */

#ifndef Device_H
#define Device_H
// #include "pugixml.hpp"
// #include <sstream>
// #include <string>
// #include <unordered_map>
// #include <vector>

class Device
{
  public:
    Device(void);
    virtual ~Device(void);

    virtual bool vReadQ(void);
    virtual bool iReadQ(void);
    virtual bool rReadQ(void);

    virtual float vRead(void);
    virtual float iRead(void);
    virtual float rRead(void);
};

#endif