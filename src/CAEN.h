#ifndef CAEN_H
#define CAEN_H

#include "CAENHVWrapper.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <iostream>

/*!
************************************************
 \class CAEN.
 \brief Class for the control of the CAEN PS.
************************************************
*/

const std::string reinitializeError = "REINITIALIZED";

class CAEN : public PowerSupply
{
  public:
    CAEN(const pugi::xml_node configuration);
    virtual ~CAEN();
    void       configure();
    void       clearAlarm();
    const int& getSystemHandle(void) { return fSystemHandle; }
    void       checkAnswer(const CAENHVRESULT& answer); // Throws an exception is answer is not ok

  private:
    void initSystem(void);
    void deInitSystem(void);
    int  fSystemHandle;
};

class CAENChannel : public PowerSupplyChannel
{
  private:
    // Variables
    CAEN*                fPowerSupply;
    const unsigned short fSlot;
    const unsigned short fChannel;

  public:
    CAENChannel(CAEN* powerSupply, const pugi::xml_node configuration);
    virtual ~CAENChannel();

    void turnOn(void) override;
    void turnOff(void) override;
    bool isOn(void) override;

    // Get/set methods
    void  setVoltage(float voltage) override;
    void  setCurrent(float current) override;
    void  setVoltageCompliance(float voltage) override;
    void  setCurrentCompliance(float current) override;
    void  setOverVoltageProtection(float voltage) override;
    void  setOverCurrentProtection(float current) override;
    float getSetVoltage() override;
    float getOutputVoltage() override;
    float getCurrent(void) override;
    float getVoltageCompliance(void) override;
    float getCurrentCompliance(void) override;
    float getOverVoltageProtection(void) override;
    float getOverCurrentProtection(void) override;
    void  setParameter(std::string parName, float value) override;
    void  setParameter(std::string parName, bool value) override;
    void  setParameter(std::string parName, int value) override;
    float getParameterFloat(std::string parName) override;
    int   getParameterInt(std::string parName) override;
    bool  getParameterBool(std::string parName) override;

    unsigned getChannelStatus();
    unsigned getParameterUnsigned(std::string parName);
    bool     isChannelRampingUp();
    bool     isChannelRampingDown();

    bool vReadQ(void) override;
    bool iReadQ(void) override;
    bool rReadQ(void) override;

    float vRead(void) override;
    float iRead(void) override;
    float rRead(void) override;

  private:
    void setName(std::string name);
    template <typename T>
    T getParameter(std::string parName)
    {
        T value;
        try
        {
            fPowerSupply->checkAnswer(CAENHV_GetChParam(fPowerSupply->getSystemHandle(), fSlot, parName.c_str(), 1, &fChannel, &value));
        }
        catch(const std::exception& e)
        {
            if(reinitializeError.compare(e.what()) == 0) // Try a second time now because in checkAnswer the connection has been reestablished
            {
                fPowerSupply->checkAnswer(CAENHV_GetChParam(fPowerSupply->getSystemHandle(), fSlot, parName.c_str(), 1, &fChannel, &value));
            }
            else
            {
              throw std::runtime_error(e.what());//It was not what I expected so throwing the exception 
            }
        }
        return value;
    }

    template <typename T>
    void setPar(std::string parName, T value)
    {
        try
        {
            fPowerSupply->checkAnswer(CAENHV_SetChParam(fPowerSupply->getSystemHandle(), fSlot, parName.c_str(), 1, &fChannel, &value));
        }
        catch(const std::exception& e)
        {
            if(reinitializeError.compare(e.what()) == 0) // Try a second time!
            {
                fPowerSupply->checkAnswer(CAENHV_SetChParam(fPowerSupply->getSystemHandle(), fSlot, parName.c_str(), 1, &fChannel, &value));
            }
        }
    }
};

#endif
