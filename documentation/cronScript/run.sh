#!/bin/bash
cd /afs/cern.ch/user/a/ancasses/eos/www/private/cms/power_supply/
git pull
git checkout main
cd /afs/cern.ch/user/a/ancasses/eos/www/private/cms/power_supply/documentation/
#doxygen config
cd
singularity exec -B /afs -B /eos docker://gitlab-registry.cern.ch/cms_tk_ph2/power_supply:doxygen bin/singularityDoxygen.sh
cd /afs/cern.ch/user/a/ancasses/eos/www/private/cms/power_supply/documentation/
rsync -avzhe ssh --progress --delete html/ /afs/cern.ch/user/a/ancasses/eos/www/private/cms/DevicesCode/
##rsync -avzhe --progress --delete html /home/antonio/cernbox/www/private/cms/MattiaCodeDoxygen/
#cd /afs/cern.ch/user/a/ancasses/eos/www/private/cms/power_supply/
#git checkout irradiationTests
#cd documentation/
#doxygen config
#rsync -avzhe ssh --progress --delete html/ /afs/cern.ch/user/a/ancasses/eos/www/private/cms/DevicesCodeIrradiationTests/
#git checkout developer
##cd /afs/cern.ch/user/a/ancasses/eos/www/private/cms/power_supply/
##git checkout irradiationTests
